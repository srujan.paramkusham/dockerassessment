﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerExample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DockerAssessmentController : ControllerBase
    {
        [HttpGet("Fibonacci")]
        public IActionResult Fibonacci(int number)
        {
            string fibonacciResult = string.Empty;
            int n1 = 0, n2 = 1, n3, i;

            for (i = 2; i < number; ++i) //loop starts from 2 because 0 and 1 are already printed    
            {
                n3 = n1 + n2;
                fibonacciResult = fibonacciResult + n3 + " ";
                n1 = n2;
                n2 = n3;
            }

            return Ok("Fibonacci Result : " + fibonacciResult);
        }

        [HttpGet("Factorial")]
        public IActionResult Factorial(int number)
        {
            int i, fact = 1;          
            for (i = 1; i <= number; i++)
            {
                fact = fact * i;
            }         

            return Ok("Factorial of " + number + " is: " + fact);
        }
    }
}
